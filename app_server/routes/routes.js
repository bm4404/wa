/* USMERJEVALNIK */
const express = require('express');
const router = express.Router();
const passport = require('passport');
const loggedin = require('connect-ensure-login');
const base32 = require('thirty-two');
const passwordless = require('passwordless');

const mongoose = require('mongoose');
let User = mongoose.model('User');

const twoFAUtils = require('./TwoFAUtils');




//**********************************************
//*             HOME and SECRET                *
//**********************************************

router.get('/', function(req, res){
    res.render('index', {title: 'Spletno overjanje'});
});

router.get('/skrivnost', function(req, res){
    if(req.session.loggedIn){
        console.log("Webauthn varianta");
        User.findOne({email: req.session.email}, function(err, user){
            res.render('skrivnost', {title: 'Skrivna stran', ime: user.name, priimek: user.surname});
        });
    }
    else if (req.user && !req.user.facebook){
        console.log("PASSWORDLESS varianta");
        User.findOne({_id: req.user}, function(err, user){
            res.render('skrivnost', {title: 'Skrivna stran', ime: user.name, priimek: user.surname});
        });
    }
    else if (req.isAuthenticated() && req.user.facebook){
        console.log("FB varianta");
        res.render('skrivnost', {title: 'Skrivna stran', ime: req.user.facebook.name, priimek: req.user.facebook.familyName});
    } else if (req.isAuthenticated()){
        console.log("Non- FB (!!!) varianta");
        res.render('skrivnost', {title: 'Skrivna stran', ime: req.user.name, priimek: req.user.surname});
    }
    else res.render('neuspesno', {title: 'Nepooblaščen dostop'});
});




//**********************************************
//*             EMAIL and PASSWORD             *
//**********************************************

router.post('/gesloRegistracija', function(req, res){
    if (req.body.email && req.body.name && req.body.surname && req.body.password) {

        // Create new user object and set hashed password
        let user = new User();
        user.name= req.body.name;
        user.surname= req.body.surname;
        user.email= req.body.email;
        user.hashValue = user.setPassword(req.body.password);

        // save user object into database
        user.save(function(err) {
            if (err) {
                return next(err);
            } else {
                console.log("User saved successfully.");
                // Avtomatsko overi uporabnika in ga prijavi!!
                let user_id = user._id;
                req.login(user_id, function(err){
                    if(err) console.log(err);
                    res.redirect('/skrivnost');
                });
            }
        });

    } else {
        console.log("Podatki manjkajo!");
        console.log(req.body.email, req.body.name, req.body.surname, req.body.password);
    }
});

router.post('/gesloPrijava', passport.authenticate('local', {successRedirect: '/skrivnost', failureRedirect: '/', failureFlash: true}));




//**********************************************
//*                 FACEBOOK                   *
//**********************************************

router.get('/auth/facebook',
  passport.authenticate('facebook', {scope: 'email, public_profile'}));

router.get('/auth/facebook/callback',
  passport.authenticate('facebook', {
    failureRedirect: '/',
    successRedirect: '/skrivnost'})
);




//**********************************************
//*                    2FA                     *
//**********************************************

router.post('/prijavaTwoFAGeslo', // post /login
  passport.authenticate('local', { failureRedirect: '/', failureFlash: true }),
  function(req, res) {
    res.redirect('/skrivnost2FA');
  });

// To view account details, user must be authenticated using two factors
router.get('/skrivnost2FA', loggedin.ensureLoggedIn(), ensureSecondFactor, function(req, res){
  res.render('skrivnost', { ime: req.user.name, priimek: req.user.surname });
});

router.get('/setup', loggedin.ensureLoggedIn(), function(req, res, next){ //get /setup
  User.findOne({_id: req.user._id}, function(err, obj) { // obj = user
    console.log("get /setup. Obj: " + JSON.stringify(obj, null, 4));
    if (err) { return next(err); }
    if (obj.key.key) {
      // two-factor auth has already been setup
      let encodedKey = base32.encode(obj.key.key);

      // generate QR code for scanning into Google Authenticator
      // reference: https://code.google.com/p/google-authenticator/wiki/KeyUriFormat
      let otpUrl = 'otpauth://totp/' + 'WebAuthn Demo' + '(' + req.user.email + ')' // app title, appTitle
      + '?secret=' + encodedKey + '&period=' + (obj.period || 30);
      let qrImage = 'https://chart.googleapis.com/chart?chs=166x166&chld=L|0&cht=qr&chl=' + encodeURIComponent(otpUrl);

      res.render('setup', { user: req.user, qrImage: qrImage, key: encodedKey });
    } else {
      // User doesn't have a key yet
      // new two-factor setup.  generate and save a secret key
      let key = twoFAUtils.randomKey(10);
      let encodedKey = base32.encode(key);

      // generate QR code for scanning into Google Authenticator
      // reference: https://code.google.com/p/google-authenticator/wiki/KeyUriFormat
      let otpUrl = 'otpauth://totp/' + req.user.email
        + '?secret=' + encodedKey + '&period=30';
      let qrImage = 'https://chart.googleapis.com/chart?chs=166x166&chld=L|0&cht=qr&chl=' + encodeURIComponent(otpUrl);

      let keyObject = {key: key, period: 30};
      User.update({_id: obj._id }, { $set: { key: keyObject}}, function(err){
        if (err) { return next(err); }
        res.render('setup', { user: req.user, qrImage: qrImage, key: encodedKey  });
      });
    }
  });
});

router.get('/login-otp', loggedin.ensureLoggedIn(),
  function(req, res, next) {
    // If user hasn't set up two-factor auth, redirect
    User.findOne({_id: req.user._id}, function(err, obj) {
      if (err) { return next(err); }
      if (!obj) { return res.redirect('/setup'); }
      return next();
    });
  },
  function(req, res) {
    res.render('login-otp', { user: req.user, message: req.flash('error') });
  });

router.post('/login-otp',
  passport.authenticate('totp', { failureRedirect: '/login-otp', failureFlash: true }),
  function(req, res) {
    console.log("Bck from totp auth");
    req.session.secondFactor = 'totp';
    res.redirect('/skrivnost2FA');
});




//**********************************************
//*                 WEBAUTHN                   *
//**********************************************

// default routes:
/* Returns if user is logged in */
router.get('/isLoggedIn', (request, response) => {
  if(!request.session.loggedIn) { // variable capitalization! loggedin =/= loggedIn
    response.json({
      'status': 'failed'
    })
  } else {
    response.json({
      'status': 'ok'
    })
  }
});

/* Returns personal info and THE SECRET INFORMATION */
router.get('/skrivnostWebauthn', (request, response) => {
  // če je logged in --> /skrivnost
  if(!request.session.loggedIn) {
    response.json({
      'status': 'failed',
      'message': 'Access denied'
    })
  } else {
    response.json({
      'status': 'ok',
      'name': request.email,
    })
  }
});




//**********************************************
//*               PASSWORDLESS                 *
//**********************************************

router.post('/brezGeslaRegistracija', function(req, res){
    if (req.body.email && req.body.name && req.body.surname) {

        // Create new user object
        let user = new User();
        user.name= req.body.name;
        user.surname= req.body.surname;
        user.email= req.body.email;

        // save user object into database
        user.save(function(err) {
            if (err) {
                //return next(err);
                console.log(err);
            } else {
                console.log("User saved successfully.");
                req.flash('info', "Registracija uspešna! Na vaš e-naslov je bila poslana povezava za prijavo");
                // notifyOfEmail("Registracija uspešna! Na vaš e-naslov je bila poslana povezava za prijavo");
                //TODO: OBVESTI UPORABNIKA, DA SE LAHKO ZDAJ PRIJAVI OZIROMA AVTOMATSKO POŠLJI MAIL (+ OBVESTILO)
            }
        });

    } else {
        console.log("Podatki manjkajo!");
        console.log(req.body.email, req.body.name, req.body.surname);
    }
});

/* POST login screen. */
router.post('/sendtoken',
    passwordless.requestToken(
        function(email, delivery, callback) {
            User.findOne({email: email}, function(err, usr){
                if (err){
                    //notifyOfEmail('Prišlo je do težave pri pošiljanju sporočila.');
                    console.log(err);
                }
                else if (!usr){
                    console.log("This user does not exist in the database!"); // obvesti uporabnika
                    callback(null, null);
                } else {
                    callback(null, usr._id);
                }
        })}, {userField: 'email', passReqToCallback: true}, { failureRedirect: '/',
            failureFlash: 'We had issues sending out this email... Could you try it at a later moment?',
            successFlash: 'You should have an email in your inbox in a couple of seconds...!' }),
    function(req, res) {
        console.log("Email sent!");
    });




//**********************************************
//*                  LOGOUT                    *
//**********************************************

router.get('/logout', function(req, res){
    req.session.loggedIn = false;
    req.session.email = undefined;
    req.logout();
    req.user = undefined;
    req.session.secondFactor = undefined;
    res.redirect('/pwdlsLogout');
});

router.get('/pwdlsLogout', passwordless.logout(), function(req, res){
    req.flash('success', "Uspešno ste se odjavili");
    res.redirect('/');
});




//**********************************************
//*                    404                     *
//**********************************************

//The 404 Route (ALWAYS Keep this as the last route)
router.get('*', function(req, res){
    res.render('neobstaja', {title: "Ta stran ne obstaja"});
});




module.exports = router;



//**********************************************
//*              UTILITY FUNCTIONS             *
//**********************************************

function ensureSecondFactor(req, res, next) {
  if (req.session.secondFactor === 'totp') { return next(); }
  if(req.user.key) res.redirect('/login-otp');
  else res.redirect('/setup');
}
