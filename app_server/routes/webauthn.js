const express   = require('express');
const utils     = require('../../config/utils');
const base64url = require('base64url');
const router    = express.Router();
const webauthn_config = require('../../webauthn_config.json'); // pomembno za Webauthn, da pozna izvor (origin)
const mongoose = require('mongoose');
let User = mongoose.model('User');

/* ---------- ROUTES START ---------- */
router.post('/registerWebauthn', (request, response) => {
    if(!request.body || !request.body.email || !request.body.name || !request.body.surname) {
        response.json({
            'status': 'failed',
            'message': 'Request missing name or username field!'
        });

        return
    }

    let name = request.body.name;
    let surname = request.body.surname;
    let email = request.body.email;

    User.findOne({email: email}, function(err, user){
      if(user && user.registered === true){ // uporabnik že obstaja in že ima registriran webauthn
        response.json({
          'status': 'failed',
          'message': `Email ${email} already exists`
        });
      }
      else {
        let newUser = new User();
        newUser.name = name;
        newUser.surname = surname;
        newUser.email = email;
        newUser.registered = false;
        newUser.authenticators = [];

        newUser.save(function(err, savedUser){
          if(err) {
            console.log(err);
          } else {
            let challengeMakeCred    = utils.generateServerMakeCredRequest(savedUser.email,
              savedUser.name, savedUser._id);
            challengeMakeCred.status = 'ok';
            // Logging in
            request.session.challenge = challengeMakeCred.challenge;
            request.session.email = savedUser.email;

            response.json(challengeMakeCred);
          }
        });
      }
    });
});






router.post('/response', (request, response) => {
    if(!request.body       || !request.body.id
    || !request.body.rawId || !request.body.response
    || !request.body.type  || request.body.type !== 'public-key' ) {
        response.json({
            'status': 'failed',
            'message': 'Response missing one or more of id/rawId/response/type fields, or type is not public-key!'
        });

        return
    }
    let webauthnResp = request.body;
    let clientData   = JSON.parse(base64url.decode(webauthnResp.response.clientDataJSON));

    /* Check challenge... */
    if(clientData.challenge !== request.session.challenge) {
        response.json({
            'status': 'failed',
            'message': 'Challenges don\'t match!'
        })
    }

    /* ...and origin */
    if(clientData.origin !== webauthn_config.origin) {
        response.json({
            'status': 'failed',
            'message': 'Origins don\'t match!'
        })
    }


    let result;
    if(webauthnResp.response.attestationObject !== undefined) {
        /* This is create cred */
        result = utils.verifyAuthenticatorAttestationResponse(webauthnResp);

        if(result.verified) {
          User.findOneAndUpdate({email: request.session.email},
            {$push: {authenticators: result.authrInfo}, registered: true}, function(err){
            if(err) console.log(err);
          });
        }
    } else if(webauthnResp.response.authenticatorData !== undefined) {
        /* This is get assertion */
        User.findOne({email: request.session.email}, function(err, user){
          if(err) console.log(err);
          result = utils.verifyAuthenticatorAssertionResponse(webauthnResp, user.authenticators);
          console.log(result);
          if(result.verified) {
            request.session.loggedIn = true;
            response.json({ 'status': 'ok' });
          } else {
            response.json({
              'status': 'failed',
              'message': 'Can not authenticate signature!'
            })
          }
        });

    } else {
        response.json({
            'status': 'failed',
            'message': 'Can not determine type of response!'
        })
    }
});





router.post('/loginWebauthn', (request, response) => {
    if(!request.body || !request.body.email) {
        response.json({
            'status': 'failed',
            'message': 'Request missing username field!'
        });

        return
    }

    let email = request.body.email;

    User.findOne({email: email}, function(err, user){
      if(err) console.log(err);
      if(!user || !user.registered){
        response.json({
          'status': 'failed',
          'message': `User ${email} does not exist!`
        });
      } else {
        let getAssertion    = utils.generateServerGetAssertion(user.authenticators);
        getAssertion.status = 'ok';

        request.session.challenge = getAssertion.challenge;
        request.session.email  = email;

        response.json(getAssertion);
      }
    });
});

/* ---------- ROUTES END ---------- */

module.exports = router;
