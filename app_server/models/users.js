const mongoose = require('mongoose');
const crypto = require('crypto');

const userSchema  = new mongoose.Schema({
    email: {type: String, unique: true},
    name: String,
    surname: String,
    randomValue: String,
    hashValue: String,
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String,
        familyName: String
    },
    key: {
      key: String,
      period: Number
    },
    registered: Boolean,
    authenticators: []
});

userSchema.methods.setPassword = function(password) {
    this.randomValue = crypto.randomBytes(16).toString('hex');
    return crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.checkPassword = function(password) {
    let hashValue = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
    return this.hashValue === hashValue;
};

userSchema.statics.findUser = function(email, done) {
    email = email.toLowerCase();
    this.findOne({ email: email }, done);
};

let User = mongoose.model('User', userSchema, 'Users');
module.exports = User;