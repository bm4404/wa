require('dotenv').load();

const createError = require('http-errors');
const express = require('express');
const path = require('path'); // not needed because built in?
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const session = require('express-session');
const expressValidator = require('express-validator');
const passport = require('passport');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const csrf = require('csurf');

const passwordless = require('passwordless');
const email = require('emailjs');
const MemoryStore = require('passwordless-memorystore');

require('./app_server/models/db'); // mongoose connection

// Init app
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

// Set public folder
app.use(express.static('./public'));

// Setup of Passwordless
const host = 'https://localhost:3000/';
const smtpServer  = email.server.connect({
    user:    process.env.OUTLOOK_EMAIL,
    password: process.env.OUTLOOK_PASSWORD,
    host:    'smtp-mail.outlook.com',
    port:   process.env.OUTLOOK_PORT,
    tls:     true
});
passwordless.init(new MemoryStore());
passwordless.addDelivery(
    function(tokenToSend, uidToSend, recipient, callback) {
        // Send out token
        console.log("TOKEN: " + host + '?token=' + tokenToSend + '&uid=' + encodeURIComponent(uidToSend));
        smtpServer.send({

            text:    'Pozdravljeni!\nDo svojega računa lahko dostopate preko:\n\n'
            + host + '?token=' + tokenToSend + '&uid=' + encodeURIComponent(uidToSend),
            from:    'webauthn.tokens@outlook.com',
            to:      recipient,
            subject: 'Žeton za WA ' + host
        }, function(err, message) {
            if(err) {
                console.log(err);
            } else {
                console.log("email sent!");
                callback();
            }
        });
    });

// Standard middleware
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Express session
app.use(session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 3 * 24 * 60 * 60 // Življenjska doba seje --> 3 dni
    }),
    cookie: {
        secure: true,
        httpOnly: true
    }
}));

app.use(flash());
app.use(require('connect-flash')());
app.use(function(req, res, next){
    res.locals.messages = require('express-messages')(req, res);
    next();
});

// app.use(csrf());
// app.use(function(req, res, next) {
//     res.locals._csrf = req.csrfToken();
//     next();
// });


// Express validator
app.use(expressValidator({ // add more sanitizers?
    customSanitizers: {
        toLowerCase: function(str) {
            return str.toLowerCase();
        },
    }
}));

// Passwordless middleware
app.use(passwordless.sessionSupport());
app.use(passwordless.acceptToken({ successRedirect: '/skrivnost' }));


// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Passport middleware
passport.serializeUser(function(user_id, done) {
  done(null, user_id);
});

passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

require('./config/passport');

// Routes
const routes = require('./app_server/routes/routes');
const webauthnAuth  = require('./app_server/routes/webauthn.js');
app.use('/', routes);
app.use('/webauthn', webauthnAuth);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

module.exports = app;
