const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const TotpStrategy = require('passport-totp').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const mongoose = require('mongoose');
let User = mongoose.model('User');
const crypto = require('crypto');


passport.use(new LocalStrategy(
  {usernameField: "email", passwordField: "password", passReqToCallback: true}, function(req, email, password, done) {
    User.findOne({ email: email}, function(err, user) {
      if (err) {
        return done(err);
      }
      if (!user  || !user.randomValue) { // to vrže vn unauthorized
        return done(null, false, req.flash('danger', "Napačno uporabniško ime ali pa uporabnik nima registriranega gesla."));//{ message: 'Napačno uporabniško ime ali pa uporabnik nima registriranega gesla.' });
      }
      password = crypto.pbkdf2Sync(password, user.randomValue, 1000, 64, 'sha512').toString('hex');
      if(password !== user.hashValue) {
        return done(null, false, req.flash('danger', "Napačno geslo."));
      }
      return done(null, user);
    });
  }
));


passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    callbackURL: process.env.FACEBOOK_CALLBACK_URL,
    profileFields: ['id', 'emails', 'name', 'displayName'] //This
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function(){
      User.findOne({'facebook.id': profile.id}, function(err, user) {
        if(err) return done(err);
        if(user) { // user found in our database
          return done(null, user);
        }
        else{ // User not found in OUR database, we need to create one.
          let newUser = new User();
          newUser.email = "null-" + profile.id;
          newUser.facebook.id = profile.id;
          newUser.facebook.name = profile.name.givenName;
          newUser.facebook.familyName = profile.name.familyName;
          newUser.facebook.email = profile.emails[0].value;
          newUser.facebook.token = accessToken;

          newUser.save(function(err) {
            if(err) throw err;
            return done(null, newUser);
          });
        }
      });
    });
  }
));


passport.use(new TotpStrategy(
  function(user, done) {
    // setup function, we need to supply key and period to the done callback
    User.findOne({_id: user._id}, function(err, obj){
      if (err) { return done(err); }
      return done(null, obj.key.key, obj.key.period);
    });
  }
));
