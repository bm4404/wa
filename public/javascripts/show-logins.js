$('document').ready(function () {

  /* PRIJAVA Z GESLOM */

  $('#switchToRegister').on('click', function() {
    $('#gesloPrijava').hide();
    $('#gesloRegistracija').show();
  });

  $('#switchToLogin').on('click', function() {
    $('#gesloPrijava').show();
    $('#gesloRegistracija').hide();
  });




  /* 2FA PRIJAVA */

  $('#switchTo2FARegister').on('click', function() {
    $('#prijava2FA').hide();
    $('#registracija2FA').show();
  });

  $('#switchTo2FALogin').on('click', function() {
    $('#prijava2FA').show();
    $('#registracija2FA').hide();
  });




  /* PRIJAVA Z AVTENTIKATORJEM */

  $('#switchToAvtentikatorRegister').on('click', function() {
    $('#prijavaAvtentikator').hide();
    $('#registracijaAvtentikator').show();
  });

  $('#switchToAvtentikatorLogin').on('click', function() {
    $('#prijavaAvtentikator').show();
    $('#registracijaAvtentikator').hide();
  });




  /* PRIJAVA BREZ GESLA */

  $('#switchToBrezGeslaRegister').on('click', function() {
    $('#prijavaBrezGesla').hide();
    $('#registracijaBrezGesla').show();
  });

  $('#switchToBrezGeslaLogin').on('click', function() {
    $('#prijavaBrezGesla').show();
    $('#registracijaBrezGesla').hide();
  });




  /* ODJAVA */

  $('#Logout').on('click', function() {
    location.href = '/logout';
  });




  /* NA DOMAČO STRAN */
  $('#GoHome').on('click', function() {
    location.href = '/';
  });
});
