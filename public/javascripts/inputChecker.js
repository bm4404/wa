function checkRegister(formName){
    let name = document.forms[formName].name.value;
    let surname = document.forms[formName].surname.value;
    let email = document.forms[formName].email.value;
    let p1 = null;
    let p2 = null;
    if(document.forms[formName].password) p1 = document.forms[formName].password.value;
    if(document.forms[formName].password2) p2 = document.forms[formName].password2.value;
    console.log(name, surname, email, p1, p2);

    if(!name || !surname || !email || (p1 === '') || (p2 === '')) {
        notify("Izpolniti morate vsa polja.");
        return false
    }

    if(p1 && (p1.length <= 7)){
        notify("Geslo mora imeti najmanj 8 znakov.");
        return false
    }

    if(p1 !== p2){
        notify("Gesla se ne ujemata.");
        return false
    }

    return true
}

function checkLogin(formName){
    let u = document.forms[formName].email.value;
    let p = null;
    if (document.forms[formName].password) p = document.forms[formName].password.value;
    console.log(u, p);
    if (p === null && !u) { // nič ni vnesel, ampak imamo samo p
        notify("Vnesite e-naslov.");
        return false
    }
    if(!u || (p === '')) { // imamo več kot u
        notify("Izpolniti morate prijavna polja.");
        return false
    } else if (formName === 'loginBG'){
        notifyOfEmail("Če je vnešeni e-naslov registriran, smo nanj poslali povezavo za prijavo");
    }
    else return true
}


function notify(msg){
    if($('#uiMessage').hasClass('alert')) {
        $('#uiMessage').removeClass().empty();
    }
    $('#uiMessage').addClass('alert alert-danger').append(
        msg + // ALERT MESSAGE
        "<button type='button' class='close' onclick='$(this).parent().hide()'>&times;</button>"
    ).show();
}


function requestUserGesture(){
    if($('#uiMessage').hasClass('alert')) {
        $('#uiMessage').removeClass().empty();
    }
    $('#uiMessage').addClass('alert alert-info').append(
        "Izvedite verifikacijsko gesto na vaši napravi." +
        "<button type='button' class='close' onclick='$(this).parent().hide()'>&times;</button>" +
        "<br><br>" /*+
        //"<button id='cancelWebauthnButton' class='btn btn-default'>Prekini</button>"*/
    ).show();
}


function notifyOfEmail(msg){
  if($('#uiMessage').hasClass('alert')) {
    $('#uiMessage').removeClass().empty();
  }
  $('#uiMessage').addClass('alert alert-info').append(
    msg + // ALERT MESSAGE
    "<button type='button' class='close' onclick='$(this).parent().hide()'>&times;</button>"
  ).show();
}
// $('#cancelWebauthnButton').on('click', function() {
//     console.log("Aborting WEBAUTHN");
//     const ac = new AbortController();
//     const acs = ac.signal;
//     return false;
// });
