//'use strict';
$('document').ready(function () {

  /* Handle for register form submission */
  $('#registracijaAvtentikatorForm').submit(function (event) {
    event.preventDefault();

    if(!checkRegister('webauthnReg')) return;

    let name = this.name.value;
    let surname = this.name.value;
    let email = this.email.value;

    // if (!name || !name || !surname) {
    //   alert('Podatki manjkajo!');
    //   return
    // }
    requestUserGesture();

    getMakeCredentialsChallenge({email, name, surname})
      .then((response) => {
        let publicKey = preformatMakeCredReq(response);
        return navigator.credentials.create({publicKey})
      })
      .then((response) => {
        let makeCredResponse = publicKeyCredentialToJSON(response);
        return sendWebAuthnResponse(makeCredResponse)
      })
      .then((response) => {
        if (response.status === 'ok') {
          //loadMainContainer()
          location.href = '/skrivnost';
        } else {
          alert(`Server responed with error. The message is: ${response.message}`);
        }
      })
      .catch((error) => alert(error))

  });


  let getMakeCredentialsChallenge = (formBody) => {
    return fetch('/webauthn/registerWebauthn', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formBody)
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.status !== 'ok')
          throw new Error(`Server responed with error. The message is: ${response.message}`);

        return response
      })
  };


  let sendWebAuthnResponse = (body) => {
    return fetch('/webauthn/response', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.status !== 'ok')
          throw new Error(`Server responed with error. The message is: ${response.message}`);

        return response
      });
  };


  let getGetAssertionChallenge = (formBody) => {
    return fetch('/webauthn/loginWebauthn', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formBody)
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.status !== 'ok')
          throw new Error(`Server responed with error. The message is: ${response.message}`);

        return response
      })
  };


  /* Handle for login form submission */
  $('#prijavaAvtentikatorForm').submit(function (event) {
    event.preventDefault();

    if(!checkLogin('webauthnLogin')) return;

    let email = this.email.value;

    console.log('user verification should have been called...');
    requestUserGesture();

    // if (!email) {
    //   alert('Email is missing!');
    //   return
    // }

    getGetAssertionChallenge({email})
      .then((response) => {
        let publicKey = preformatGetAssertReq(response);
        return navigator.credentials.get({publicKey})
      })
      .then((response) => {
        let getAssertionResponse = publicKeyCredentialToJSON(response);
        return sendWebAuthnResponse(getAssertionResponse)
      })
      .then((response) => {
        if (response.status === 'ok') {
          //loadMainContainer()
          location.href = '/skrivnost';
        } else {
          alert(`Server responed with error. The message is: ${response.message}`);
        }
      })
      .catch((error) => alert(error))
  });

});
